angular.module('webphone');

angular.module('webphone')
  .controller('CallController', function($timeout, $interval, $rootScope) {
      var vm = this;
      var simple = null;

      
      activate();
      initSip();
      addListeners();
      function activate() {
        vm.showCallPanel = true;
        vm.callInfo = {
          firstName: '',
          lastName: '',
          phone: '',
        }
        vm.callStatus = ''; //idle, ringing, connected, connecting, 

        vm.isMute = false;
        vm.isHold = false;
        vm.accept = accept;
        vm.reject = reject;
        vm.hangup = hangup;
        vm.keypads = ['1','2','3','4','5','6','7','8','9','*','0','#'];
        vm.dtmf = dtmf;
        vm.counter = 0;
        vm.counterText = '';
        vm.toogleMute = toogleMute;
        vm.toogleHold = toogleHold;
        
      }
      function initSip(registerInfo) {
        console.log('Thong tin la:', registerInfo); //dong nay de em kiem tra da vao identity va pass
        var options = {
          media: {
            local: {
              audio: document.getElementById('localAudio')
            },
            remote: {
              audio: document.getElementById('remoteAudio')
            }
          },
          ua: {
            uri: registerInfo.identity +'@tel4vn.com',    //cho nay em de cho anh lam lai thu
            authorizationUser: registerInfo.identity,
            password: registerInfo.password,
            wsServers: ['wss://vn01.tel4vn.com:7444']
          }
        };
      }

      function addSipEvents(simple) {
        simple.on('registered', function (event) {
          console.log('event registered');
          console.log(event);
        })
        
        simple.on('unregistered', function (event) {
          console.log('event unregistered');
          console.log(event);
        })
        simple.on('ringing', function (event) {
          console.log('event ringing');
          console.log(event);
          $timeout(function() {
            vm.callInfo.phone = event.remoteIdentity.uri.user;
          });

          updateStatus('ringing');
        })

        simple.on('connecting', function(event) {
          console.log('event connecting');
          console.log(event);
          updateStatus('connecting');
        })
        simple.on('connected', function(event) {
          console.log('event connected');
          console.log(event);
          updateStatus('connected');
          startTimer();
          
        })

        simple.on('ended', function(event) {
          console.log('event ended');
          console.log(event);
          updateStatus('');
          vm.callInfo = {
            firstName: '',
            lastName: '',
            phone: '',
          }          
          stopTimer();
          $timeout(function(){
            vm.counterText = '';
          },500)
        })

        simple.on('mute', function(event) {
          console.log('event mute');
          console.log(event);
        })

        simple.on('unmute', function(event) {
          console.log('event unmute');
          console.log(event);
        })

        simple.on('hold', function(event) {
          console.log('event hold');
          console.log(event);
        })

        simple.on('unhold', function(event) {
          console.log('event unhold');
          console.log(event);
        })

        simple.on('dtmf', function(event) {
          console.log('event dtmf');
          console.log(event);
        })
      }


      /**internal */
      function updateStatus(status) {
        $timeout(function(){
          vm.callStatus = status;
        });
      }
      /* buttons */
      function accept() {
        simple.answer();
      }
      function reject() {
        simple.reject();
      }
      function hangup(){
        console.log('console hangup');
        simple.hangup();
      }

 

      function toogleMute(){
        if(vm.isMute){
          //unMute
          simple.unmute();
        }else {
          simple.mute();
        }
        vm.isMute = !vm.isMute;
      }

      function toogleHold(){
        if(vm.isHold){
          simple.unhold();
        } else {
          simple.hold();
        }
        vm.isHold=!vm.isHold;

      }
      function dtmf(key){
        console.log('dtmf: ', key);
        simple.sendDTMF(key);
      }

      function addListeners() {
        $rootScope.$on('CALL', function(event, contact){
          simple.call(contact.phone);
          vm.callInfo = contact;
        })
        $rootScope.$on('REGISTER', function(event, registerInfo){
          console.log('REGISTER EVENT', registerInfo);
          initSip(registerInfo);
        })

        
      }

      
      function startTimer(){
        intervalHandler = $interval(function() {
          vm.counter++;
          vm.counterText = counterToDuration(vm.counter);
        }, 1000);
      }
      function stopTimer(){
        $interval.cancel(intervalHandler);
        vm.counter = 0;
      }
      function counterToDuration(counter){
        //counter /3600 = hours
        //(couter - hours*3600)/60 = minute
        //couter - (hours*3600) - (minute*60) = seconds
        var hours = 0, minute = 0, seconds = 0;
        hours = Math.floor(counter/3600);
        minute = Math.floor((counter - (hours*3600))/60);
        seconds = counter - hours*3600 - minute*60;
        

        if(hours < 10){
          hours = "0" + hours;
        }
        if(minute < 10){
          minute = "0" + minute;
        }
        if(seconds < 10){
          seconds = "0" + seconds;
        }
        if (hours == 0)
        {
          return minute+":"+seconds; 
        }
        else {
          return hours+":"+minute+":"+seconds;
        }
        
      }
      
     
  });