angular.module('webphone',[]);

angular.module('webphone',[])
  .controller('ContactController', function($rootScope) {
    var vm = this;
    activate();
    function activate() {
      vm.isShowingButtons = [];
      //vm.isShowingButtons[0] = true
      //vm.isShowingButtons[1] = true
      
     
      vm.contacts = [
        {
          firstName: 'Huy',
          lastName: 'Do',
          phone: '502',
        }, {
          firstName: 'An',
          lastName: 'Ha',
          phone: '503',
        }, {
          firstName: 'Trung',
          lastName: 'Nguyen',
          phone: '504',
        }, {
          firstName: 'Xuan',
          lastName: 'Thang',
          phone: '505',
        },
      ]
      vm.showEventButtons = showEventButtons;
      vm.showRegisterModal = false;
      
      vm.addContact = addContact;
      vm.call = call;
      vm.delContact = delContact;
      vm.editContact = editContact;
      vm.register = register;
      vm.registerInfo = {
        identity: '',
        password: ''
      }
    }
    function register() {
      console.log('console register', vm.registerInfo);
      $rootScope.$broadcast('REGISTER', vm.registerInfo);
   }
    function showEventButtons(index, flag) {
      vm.isShowingButtons[index] = flag;
    }
    
    function addContact() {
      console.log('addContact function');
    }
    function call(contact) {
      console.log('call function', contact);
      $rootScope.$broadcast('CALL', contact);
    }

    function delContact(contact) {
      console.log('delContact function', contact);
    }
    function editContact(contact) {
      console.log('editContact function', contact);
    }
    
  });